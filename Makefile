SoftSec.pdf: src/*.md  
	pandoc src/*.md -o SoftSec.pdf --table-of-contents -V documentclass=report -V geometry:margin=2.5cm --number-sections -s 
clean:
	rm -f SoftSec.pdf 
	#pandoc cleans .tex files automatically
