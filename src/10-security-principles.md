# Security principles

Several papers propose *checklists* of security principles: security vulnerabilities often exploit violations of these principles, and countermeasures can be designed by following them.
However, some principles may contradict each other when followed to the logical extreme; in general, the best result may require compromise.

- secure weakest link
- defence in depth
- least privilege possible
- minimise attack surface
- compartmentalize
- secure defaults
- KISS (keep it simple, stupid)
- fail securely
- promote privacy
- no security by obscurity
- use community-built, tried and tested resources
- be reluctant to trust

## Securing the weakest link

Improving the safest parts of the system is often useless if it will be bypassed by exploting other parts. Still, securing the weakest link requires proper **risk analysis** as each part should be hardened according to both likelyhood and consequences of a breach.
Often, the weakest link is the *user*, therefore educating users to best practices might be the best investement.

## Defence in depth

Systems should not rely on a single defence mechanism, i.e. have a **single point of failure**. The more controls are in place, the less likely an attacker will be able to compromise all of them at the same time.

> **Example**: restrict access to key files at OS level, *and* encrypt them

> **Counterexample**: trusting a firewall (with no other security mechanism) to guarantee security - this can be easily beaten by an attacker walking into a company site with a laptop (a kind of **environmental creep**).

## Principle of least privilege

Only grant permissions that are actually necessary to carry out a task, and only for the duration of said task.
Often, this is not done due to lazyness, however assigning *very* specific permissions is hard, which implies easy to get wrong, and therefore against KISS.

> **Counterexamples**: 
> - logging in as root
> - run webapps as root
> - device drivers that run on kernel mode
> - giving everyone in the organization admin credentials
> - *not* disabling *reflection* in java

## Compartmentalize

If system is divided in chunks, a breach in a subsystem may be *contained* there. It can also simplify access control policy.

Can also be applied to code, i.e. **modularization**.

> **Counterexample**: OS should not crash if an application crashes
> 
> **Examples**: 
>
> - use different accounts, different partitions, or even better different machines, for different tasks
> - use **jails** like `chroot` to restrict a process to view and access only a subset of the filesystem
> - use **virtual machines** and **containers**
> - use **modularisation** in code

## Minimize attack surface

Unneeded services and functionalities should be disabled, as they would only serve the attacker in presenting additional ways of entry. 

> **Counterexamples**
> - Using `public` rather than `private` or `package` modifiers in java (also principle of *least privilege*)

Additionally, **time** in which the system is potentially vulnerable can also be considered an attack surface to minimize:

> **Examples**:
> 
> - automatically log off users after some time
> - lock screen
> - disable network if unneeded
> - switch off idle devices


## Secure defaults

All security features should be turned on by default and all permission should be absent if not specifically set. Failing to do this will almost certainly create a case where someone eventually forgets to activate some secrity feature or remove an unneded permission which then gets exploited.

> **Example**: new users created through `useradd` in Linux are not usable until a password is set for the user. 

## KISS

Excessive complexity causes unpredictable interactions between features and components, and incorrect use by users.
Good practice is to enable *choke points* for all control flows.

## Fail securely

Incorrect handling of unexpected errors has often been exploited.

> **Counterexamples**:
> 
> - fallback to *unsafe* modes upon failure (often for compatibility with legacy software)
> - asking user to decide how to remedy security problems
> - crashing the machine on application errors (easy DOS attacks)
> - leaking information useful to an attacker through error messages or response time (authenication fail, blind SQL injection returning parameter type info, etc)
> - ignoring errors, misinterpreting errors, handling wrong or *all* exceptions.
> - using *blacklists* instead of *whitelists*

## No security by obscurity

Don't assume attackers won't be able to obtain "secret" information like source code, for example through reverse engeenering.

> **Counterxamples**:
>
> - relying on code obfuscation
> - hiding secrets hardcoded into the source code, like emergency credentials (this is done by a huge number of companies for many devices)

## Use community resources

Only use tried and tested software whenever possible.

> **Counterexamples**:

> - Don't design proprietary cryptographic systems nor new implementations.
> - Don't repeat past (known) mistakes

## Psychological acceptance

If security mechanism is too complex and cumbersome, users will *disable* it or find ways around it, regardless of training.
If security alerts are constants, users will *ignore* them.

## Don't mix data and code

Traditional cause of common attacks such as buffer overflows, SQL injection etc.

## Clearly assign responsibilities

- In organizations, assign responsibilities to a single person
- In code, validate input through a single module/class (**chokepoint**)

## Identify assumptions

Assumptions, even trivial, may change over time: this should be noted explicitly.
Most attacks work by **invalidating security assumptions**.

## Be reluctant to trust

- Minimize **TCB** *(Trusted Code Base)*.
- Assume *all* user input is dangerous.
- Do *not* assume data from third party software is safe.

> **Famous counterexample**: Thompson hack/"trusting trust attack"
> Ken Thompson revealed a trojan he succesfully inserted in the C compiler, and all code compiled with it (including new versions of the compiler!), during his Turing award lecture.
