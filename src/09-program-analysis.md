# Program Analysis

> The later a vulnerability is found, the more expensive it is to fix it.

**Definition**
Developing algorithms and tools to **automate** the analysis of other programs.

**Aims**

- Bug finding
- Security
- Verification of program specification
- Compiler optimizations
- Automatic parallelization

**Types**

- **Dynamic analysis**: on program execution
- **Static analysis**: on the source code
- **Hybrid**: uses both
- **Operational**: done after software release 
- (on byte code)


## Static analysis

- Less precise as it will often flag cases that cannot actually happen
- More general compared to a single (or few) execution.
- Can utilize variable names and comments, which may help significantly
- Can potentially fix problems directly since it has source code access


### Human analysis

- Humans great at discerning context and **intent**
- Long and repetitive job, easily boring => high probability of leftover vulnerabilities
- Very expensive (human time)
- Need peer reviews


### Automatic tools

- Automated means less expensive
- Tools don't have an innate understanding of architecture, goals, environment
- Usually have significant rates of FP and/or FN.
- Best used in conjunction with other methods


#### Precision measurements of classifiers

- Precision: true positive rate, i.e. $\frac{TP}{TP+FP}$
- Recall: find rate, i.e. $\frac{TP}{TP+FN} = \frac{TP}{\text{All positives}}$
- F-score: harmonic mean, i.e. $\frac{2*(Prec * Rec)}{Prec+Rec}$


#### Security defects text scanners

Simple programs that scan source code, grep-like, for known security problems. Fast and cheap, work on partial code, but large FP/FN rate (they are context-insensitive). 

Examples: `RATS`, `Flawfinder`.

#### Secrutity defects finders

More complex programs that read and create an internal model of the software and its data flow.

Ex: `Splint`.

#### Property checkers

Aim to *prove* that a specific property is true throughout the program. Usually, the property has to be very narrow and temporal, e.g. "program always frees allocated memory".

#### General purpose tools

Tools that are *not* specifially aimed at security can still be valuable:

- may find bugs
- may incentivize to produce better code, easier to analyse and secure
- usually faster, cheaper, easier to use


#### Static type-checking

Very useful for *documenting intent*, and can detect or prevent some security problems at compile time. To exploit their benefits types should be as narrow as possible, but this has diminishing returns.

#### Other warnings

Can also be useful and should be fixed early in development to reduce costs:
- **Compiler** warnings
- **Runtime** warnings.

#### Style checkers

They compare code to a set of premade patterns, rules or probable defects in order to flag patterns that usually lead to bad code. Focus is on general code quality, not security.

### Taint propagation analysis

Consists in marking as **tainted** all input from untrusted sources, namely users, in order to warn whenever such data is consumed by certain functions (**sinks**). All computations on tainted data also output tainted data (*pass-through*).
Some methods can *untaint* data (usually by validating the input).


### Main problem of static analysis

> Given source code P and desired property Q, does P exhibit Q in **all possible executions**?

This problem is **undecidable**, therefore static analysis is either:

- **Unsound**: includes false negatives
- Sound but **incomplete**: includes false positives
- **Non-terminating**, in which case it's useless 

The only useful analysers, in practice, are those that are sound but incomplete: *some* false positives are acceptables.

### How to design static analysis tools

Abstract program behaviour (i.e. overapproximate) so as to partition the possible states in acceptable and unacceptable. This approximation is composed of **abstract domains**, set of abstract values that we want to track depending on the particular feature being tested.

> **Example**
> It may only be relevant to track whether the output of any arithemtic operation is non-zero rather than its actual result.

**Abstract domains** can then be be used to define:

- **Abstraction function $\alpha$** : maps concrete values to A.D.
- **Concretization function $\gamma$** : maps a domain to its concrete values, and defines a partial order between abstract values using their concretized value.
- **Abstract Trasformers** may also be needed for operations on domain values (e.g. *sign analysis*).

Abstraction and concretization are "almost inverse" operations (Galois insertion [?]).

Notable special symbols for domains:

- $\top$ (top) = any value
- $\bot$ (bottom) = no value, i.e. empty set

The construction can either

- start very broad and then shrunk until possible (until it excludes acceptable terminating states) 
- start very small and expand until it recognizes all acceptable terminating states.

### Fixed point computation 

The verification starts by assigning A.D. to variables, then executes the program instruction by instruction, updating A.D. after each one. In case of branches, computation continues on all possible cases, then union A.D. are computed at joining points. In case of loops without exit clause, the verification continues until no changes are made to the domains of the variables.

Computation always terminates as long as A.D. form a **complete lattice**, i.e. every subset of elements has a LUB [?].

## Dynamic Analysis

- Veryfing executions for *all* possible inputs is generally **impossible** in practice, unless only few specific (programmer-defined) options are accepted.
- Easy to show something can happen, hard to show something *never* happens, which is usually required by security policy.


## Coverage measures

Attempt to measure the % of code that has been analysed, e.g. covered by at least one test:

- statement coverage: line of code covered by at least one test
- branch coverage: program branches covered by at least one test (including branches that have no statements)


### Functional testing for security

Normal testing approach should be supplemented by tests for behaviours that both *should* and *should not* happen according to security policy.

#### Web application scanners

One approach often used for web-apps is to scan the user-space surface for vulnerabilities.

#### Fuzz testing

Testing technique that consists in providing large numbers of invalid input, usually obtained by minor, random modifications on valid data.

## Hybrid approaches

### Penetration testing

Try to break security policy pretending to be an adversary, but following some **Rules of Engagement**.

> **Problem**: RoE usually unrealistic (too strict).

### In-production analysis

- logging
- sandboxing
- firewalls, NATs, other network-based tools

