# Security engineering for software

Mistakes that potentially cause vulnerablities can be caused at any step of the development lifecycle:
1. Design flaws
2. Coding flaw
3. Oversights in code inspections/testing
4. Regressions in maintenance
5. ?

## Software engineering
Creating and maintaining software applications by applying principles..?

Traditional software development cycle does not consider security:
Requirements $\rightarrow$ Design $\rightarrow$ Implementation $\rightarrow$ Testing $\rightarrow$ Deployment $\rightarrow$ Maintenance

Because of this, many flaws are identified only in the late stages, at considerably higher cost.

## Building secure software

Pillars of writing secure software:

1. risk management
2. touchpoints (important points to consider?)
3. knowledge

### Risk management

Should be conducted as early as architecture design, but might have to be conducted multiple times at different stages.
It takes place in a given business context and is therefore affected by its motivation.
It should be noted that this process is an estimate largely based on historical knowledge.

Risk consequences may be:

- direct financial loss
- reputation loss
- violation of regulations
- increased development costs
- etc

Their severity should be captured in financial terms or project management terms, and will depend on:

- likelyhood
- impact

Mitigation strategies should be weighted on:

- cost
- time
- completeness
- impact
- likelyhood of success

and should also include a method of measuring the effectiveness.
From a business point of view, it may turn out that buying insurance is more effective against a particular risk than trying to completely prevent it, for example.


#### A proposed framework

1. Understand business context
2. Identify business and technical risk to the company
3. Synthesize and rank risks
4. Define risk mitigation strategy
5. Validate and deliver fixes where needed


### Touchpoints

1. code reviews
    - input validation
    - API abuse
    - security features
    - time/ state (race conditions, deadlock, TOCTOU...)
    - error handling
    - code quality
    - encapsulation
    - environment
2. architectural risk analysis
3. penetration testing
4. risk-based security tests
5. abuse cases
6. security requirements
7. security operations

### Knowledge

- Prescriptive
- Diagonostic
- Historical

### Attack resistence analysis

This process requires knowledge of possible attacks, usually based on history.

- identify flaws using design literature and checklists
- map attack patterns using a list of possible attacks, or past abuse cases
- identify risk based on checklists
- demonstrate the viability (or lack thereof) of known attacks using exploit/attack graph

Need to consider also known weaknesses of third-party software, environment, platform the software uses/runs on.

### Penetration testing

Common way to test the system at the final step, done by a third party, but often it is *too little, too late*.
Better protocol:

- test from the beginning and throughout the whole lifecycle
- base testing on risk
- conduct it internally, in order to reduce the need for extraordinary limitations in *rules of engagement*

### Abuse cases

It is important to define *anti-requirements*, i.e. things the system should *not* do.
Should construct attack patterns for *any* person who could gain access to the system, not just the intended user.
Should also consider the possibility of **insider threats**.


# Capability Maturity Models

> **CMM Idea**
> The software produced must be good if the process is sound.

There are organizations that evaluate and certify, against given standards, the process of businesses that develop software.

## Main organizations

- CMMI *(CMM Integration)*
- iCMM *(integrated CMM)*
- SSE-CMM *(Systems Security Engineering CMM)*

Older:
- TSM (*Trusted Software Methodology)*

### SSE-CMM

> "Security is part of engineering"

129 base practices, 22 process areas. They don't overlap and do not consist in applying certain tools.

- Project and organizational process *(68)*
  - .....
- Security engineering process areas *(61)*
  - .
    - ..
  - .
    - ..
  - .
    - ..
  
#### Measured capability levels

0. Not performed
1. Performed informally
2. Planned & tracked
3. Well defined
4. Quantitatively controlled
5. Continuously improving

#### SSAM *(SSE-CMM Appraisal Method)*

...?

## CMM problems

> **CMM Problem 1**
A reference model does not present **operational guidance**: defines what can be improved, not *how*.

### Secure process

Set of activities performed to develop, maintain and deliver secure software

### Process model
A reference set of best practices for **process assessment** and **process improvement**.

> **CMM Problem 2**
>  High-graded process is not in fact a guarantee for software quality of all (or even *any*) product.

## Main SDLC areas

- **security engineering**: activities needed to design a secure solution.
- **security assurance**: verification, validation, external expert reviews and other activities that increase trust in the produced *artifacts*.
- security focused activities 
  - **organizational** management
  - **project** management
- security **risk identification and management**
  - cost-based RA
  - risk mitigation

