# Java security

***Note**: most of this also applies to C#.*

## Guidelines

### Least privilege

Limit access to classes, methods and fields. Use *final* classes and *sealed* packages so that neither can be extended and redefined maliciously, but still do not rely on package protection for security: use *private* rather than *protected* fields whenever possible. Additionally, use *final* fields for thread-safety.

`JAMIT` tool tests for visibility that is not actually used and may therefore be restricted at no cost.

***Note***: many of the above principles are only actually relevant for APIs and application components that are (or may in the future be) **extended** with less trusted code. But context change is a frequent cause of vulnerabilities in previously safe components.

#### Inner classes

Inner classes are supposed to be a way to encapsulate part of a class that should *not* be visible outside. However, the bytecode compiler makes **no distinction** between inner and outer classes. Therefore, methods contained in the inner class may offer publicly accessible methods to private fields of the outer class.

#### Cloneable class instances

Cloning a class may be a way to bypass rules and checks implemented inside constructors for the class. If that could be a problem, then cloning objects should be prevented.

#### Serialization

Serialization means objects could be serialized, modified outside the class structure (potentially bypassing checks) and then de-serialized. Not implementing serialization of objects prevents this problem.

#### Reflection

Ability of java programs to inspect & modify themselves, including redefining methods and changing visibility (if given permission to). May render all previously mentioned safeguards moot, therefore should be disabled if security is a concern.

### Mutable objects & aliases

- Never return a reference to a mutable object to untrusted code
- Never store and use a reference to a mutable object from untrusted code

For example, arrays should be cloned rather than referenced to prevent **representation exposure** to unintended or malicious changes in the original array.

## Mobile code

### Host security

Protect user from unreliable code

- confidentiality
- integrity

### Code security

Protect program from untrusted user - this is not implemented in Java: need information flow control in the host.

### Additional anti tamper measures

Additional measures can be taken to reduce danger of untrusted bytecode:

- Re-compile from source (if available)
- Certified compilation, i.e. add a certificate from a trusted compiler to the code
- Check bytecode for tampering
- Proof-carrying code: the code includes a proof that can be verified (easier than checking bytecode itself).

## The Java Virtual Machine (JVM)

Code is not executed directly on hardware, but as bytecode run on a software layer, the Java Virtual Machine.
This adds portability as bytecode is not dependent on computer architecture (while the JVM itself can be optimized for each platform), but security checks are necessary to ensure the bytecode has not been corrupted (or maliciously modified) after compiling.

The JVM is composed of:

- Byte-code verifier: anti-tamper checks
- Class loader: manages dynamic code loading and access rights of classes
- Access controller: manages access rights at runtime


### Type safety

Type safety is verified first at compile time (statically), then checked at runtime (dynamically) everytime type assignments and coercions are made.
If no exception is raised, Java guarantees **type preservation**, i.e. runtime types will match declaration types even after computations are made (this is not true in C, where e.g. a `char*` actually points to an int).

#### Lattice of Java types 

Types have a **lub** (least upper bound) and a **glb** (greatest lower bound). 

A set of types $A$ form a **lattice** ($A$, $\le$ ), i.e.:

1. $\le$ is a partial order (reflexive, anti-symmetric) on $A$
2. $A$ has a maximal element (top) $\top$ and a minimal (bottom) element $\bot$
3. Each subset of $A$ has a lub and a glb on $A$

At runtime, types are inferred from bytecode using the **lub** to **unify distinct types** (top or bottom results in an error, as it means the variable cannot be safely typed).

### Memory safety

Compiler and runtime controller both checks that memory access is only made to valid objects, such as within array bounds. No direct memory access is provided, and memory deallocation is managed by a **garbage collector**.

### Control flow safety

Compiler and runtime forbid *arbitrary code jumps* by imposing **structured control flow**: methods can only be accessed through their entry points. Additionally, access to OS method is restricted to those implemented by the VM.

### Byte-code verifier

- Type safety
- Memory safety
- No stack overflow within a method call
- Inizialization of objects before use

In byte code, **all instructions are typed**; **stack** and **registers** are also typed, to catch any potential type mismatch.
To accomplish this, each assembly-like command has variants for each type noted by prepending a single-letter short hand, e.g. `iadd` sums integers.


#### Example

cmd | registers | stack
-|-|-
r0: int, r1: A, r2: B, r3: T, r4: $\top$ | |<>
iload r0 | |< int >
*ifeq* |
> iconst 42 | |< int, int >
> istore r4 | r4: int| < int >
> aload r1 | |< A, int >
*else* | |
> aload r2 | |< B, int >
*end* | r4: $\top$| < A, int >
iload r4 | | **error**: r4 is type A, not int

### Class loading

Classes are loaded dynamically on-demand (**lazy loading**). This is a security-critical process, as failing to validate classes could result in loading and potentially executing arbitrary external code that was crafted to look like an expected class, therefore it needs to be **access controlled**.

It is actually done by a **hierarchy** of **class loaders** built using *least privilege* principle. This provides isolation to, e.g., applet classes loaded for use in the browser.

#### Dynamic loading process

- Check if requested class has already been loaded
- Otherwise load the class
- Read & build class code
- Link edition (?)
- Call verifier

#### Loaders hierarchy

1. **Primordial class loader**, bootstrapped in C, deals with the OS.
2. Java.lang.Classloader
3. Java.security.ClassLoader
4. *...secured class loaders...* 
5. Java.net.URL
   - .ClassLoader
   - .AppletClassLoader

## Access control

Access manager receives access requests from class loaders, and grants or denies the resouce based on the security policy. Note that only the execution of methods is actually controlled [?].

### Permission

$$p_i \equiv (r_j, O) \quad  \text{with } r_j \in Resources,\quad O \subseteq Operations$$

Though a single resource can actually include many "lower level" resources, e.g. an URL for a directory that contains several files.

Each class can be assigned any number of permissions, and naturally more general permissions include more specific ones.

### Access control manager

The manager needs to be provived at compile time, otherwise the default is *no control*.
Default policy in *java.?.policy*.

Java implements a **stack walking** algorithm: by default, operation $o_j^{r_i}$  will be allowed only if *all* methods on the stack have permissions for $o_j$ on $r_i$ (i.e. **intersection** of rights), and only the access rights of the current class are counted (i.e. not those of the class of any external method called by the current class's method),but there are ways to temporarely gain (or lower!) privileges.

#### Privileged mode

Callee may give temporary rights to the caller. In this mode, permission is granted without further traversing of the stack since the callee is guaranteeing for the privileged method. 
This can be used to give permissions for operations that a component has access to, but that would be prevented due to the component's intended callers (e.g. a user-facing class) not having said permission. 

#### Limitations

- No direct link with OS's access control functions.
- No fine-grained dynamic memory management.
- Relies on the security of the appropriate library for the JVM (includes some C/C++ code).

