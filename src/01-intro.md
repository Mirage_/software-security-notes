---
title: |
    | Notes:
    | Security in Software Applications
author:
- Andrea Castellani
date: Fall 2019
---

# Info (2019) {.unnumbered}

**Start time**: 8.15  
Course [Homepage](http://wwwusers.di.uniroma1.it/~parisi/SoftwareSecurity_fa2019.html) | [Moodle](https://elearning.uniroma1.it/course/view.php?id=7654)  
**Office Hours**: tue/wed afternoon  
**Final grade**: 40% written exam, 30% group project, 2x 15% homework
**Group Project**: 2-3 people.

This document in its most up to date version can be found at <https://gitlab.com/Mirage_/software-security-notes>, along with source code and makefile.

# Intro

**Safety**: program protected against accidental failures  

**Security**: program protected against malicious attacks

## Vulnerabilities

A flaw in the program that me be used to breach the **security policy**.
Not all vulnerabilities are *liabilities* - i.e. exploitable in a given context - but the context is likely to change at some point (new modules, new environment...), potentially exposing the system.

**Security** in the real world: maintaining what the security policy says.  

> **Problem**
> Describing what the software is *not* supposed to do is surprisingly hard.

**Bugs** are incorrect program behaviour that may or may not *not* breach the secuirity policy. 

**Tools** can find many, but not all vulnerabilities.

It is virtually impossible to know for certain that a bought piece of sotware does *not* have **insider threats**. Often, what is actually sold it's the *appearance* of security.

Additionally, software is usally tested only in *ideal* conditions, rather than assuming the user will try (apparenty) stupid actions.


#### Vulnerability *(alternative definitions)*

- A bug with security consequences
- A design flow that may allow attackers to use the software in malicious ways
- A software flaw which can be exploited

Traditionally most were in the OS; nowadays attackers mainly exploit web applications.

#### Flaw

Part of the software that is wrong, or could be more secure, but it is not *necessarily* exploitable in a given **context**.

- Some can be identified by looking at the program
- Some depend on the **interaction** with underlying platform or other external systems (e.g. integer overflows)


#### Exploit

Software or sequence of commands that takes advantage of a vulnerbaility to cause unintended or unexpected behaviour

**Threat**: 

## Examples

### C problems

Programmer can access the memory directly - this is the main cause of problems.  

Examples:

- **free()**: called twice can cause damage
- **strlen, strcopy**: often destination is too small, as the input comes from the attacker, which can lead to injection of arbitrary code.
- **strlen** is supposed to give an answer, but requires an opportune string termination. If missing, the output will be misleading.

### Java problems

Keywords **public**, **protected** may allow attackers to modify the intentional behaviour of the program in malicious ways through subclasses. **Final** is much safer, but constrains developers.

In theory, Java does *not* allow buffer overflows since it implements **dynamic bound checking**, but the bytecode is interpreted by the Java machine, which written in C, so there may be overflows *there*. Therefore, even Java cannot guarantee the absence of buffer overflows, though they would generally be much harder to exploit.

#### Type-mismatch in Java Virtual Machine

In previous versions it was possible to confuse the Java machine by creating classes with names such as `A[]` so that the machine wouldn't understand whether said class was being called, or an array of class `A` initialized.


## Countermeasures

#### Types of countermeasures:

- Anti-virus, intrusion detection/prevention, etc
- Cryptography
- Acess Control policies
- Can also be unrelated to IT: e.g. **prosecution** by law

The type of protection deployed for software *must* be related to the **importance of the data bing protected**, and the **probability** of attack.

**Patches**: updates meant to remove vulnerabilities. They can actually be used as a blueprint to *craft* attacks, since most users don't patch the system immediately, or often enough anyway. They might also introduce *new* vulnerabilies.

**OWASP**: paradigm that tries to verify software through a set of rules

**Producing secure software** requires a focus on security throughout the whole process of design, development and distribution.

**Type systems** can be used to ensure that the information flow is not improper, e.g. **JEF**, **PCC** (Proof Carrying Code, code that carries a proof that can be verified in polinomial time).

**Code Obfuscation**: technique meant to make reverse-engineering more difficult, usually targeting the main tools developed for it.


## Concepts

#### Covert channels

Means of communicating that weren't originally meant for such use. Speed of the channel is key, therefore it is often enough to slow down the channel rather than shut it down, to render it ineffective.  

#### Confidentiality

Currently "solved" by cryptography. The basis of current crypto is prohibitive time-comsuming computations, not real absolute security, therefore *quantum computers* could render it all ineffective. Additionally, their security relies on the security of any **PRNG (Pseudo-random Number Generator)** employed.

#### Integrity

The main concern of secure software development. Nothing should be *trusted* by default, including cryptographic systems. Even if an application is secure, attackers could emulate the real software with a malicious copy (e.g. keyloggers).

#### Logic bomb

Code that only executes in certain situations, planned ahead for by the attacker. Historically, often used by employees to get back at their (past?) employer.

#### Insider threats

Often, the most realistic danger to organizations are attacks from within their infrastructure or within their personnel, since most security features can be circumvented or deactivated if (unsupervised) physical access is possible.

#### Types of attackers

- **amateurs**: mainly hack for fun & prestige (publishing findings)
- **professionals**: work for profit, for hire (organized crime, governments).
