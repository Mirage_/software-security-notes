# Testing

Need

- test **suite**, collection of data to feed the oracle
- test **oracle**, something which determines whather the test is positive

A very simple oracle may just record whether the applictaion crashes. A complex oracle may be composed of a long list of specification cases to verify case-by-case.

## Code coverage criteria

- **Statement** coverage: statements (%) that are executed by at least one test 
- **Branch** coverage: program *branch options* (%) executed by at least one test

> **Problem**
> High coverage criteria may *discourage* defensive programming, since code included for safety may be difficult to trigger in tests and project may have a required minimum coverage.

## Oracle

May use **annotations** in source code (C++'s *SAL*, Java's *JML*) to perform runtime **assertion checking** on random input data.

Runtime checking of information flow requires more complex strategies. For example, Android's `SPARTA` uses **dynamic taint tracking**, i.e. is capable of tracing the origin of data within the application.

## Security testing

Much more complex: normal testing looks at correct (i.e. **intended**) input and possibly edge-cases, therefore it is more likely to find **functional** problems.
Security testing instead looks for wrong (**unwanted**) input in order to find security problems.
Additionally, users effectively test the application every time they use it, and are likely to report bugs/complain if something does not work; attackers don't complain about the vulnerabilities they exploit. 

### Abuse cases

>**Example**: [iOS *goto fail* SSL vulnerability](http://www.dwheeler.com/essays/apple-goto-fail.html)

Caused by a simple bug where a `goto fail` instruction outside its correct check condition causes program to skip error handling by being always executed.

- *FrankenCert* suite provides broken certificate chains to use for testing errorhandling for certificate flaws.
- Could and should have been prevented simply by compiler warning for unreachable code.

### Fuzz testing

[Miller & Wisconsin (1988)]() found they could crash >25% of UNIX utilities through long random inputs, due to missing error checking, array bounds checking etc.

#### Simple examples 

- very long strings
- empty strings
- Max/min int values
- Zero or negative int values
- Characters or keywords dangerous for the given language

**Pros**
- Little effort: auto-generated test cases (oracle looks for crashes)
- Clear **true positives**

**Cons**
- Will *not* find all the bugs (many **false negatives**)
- Hard to analyze crashes 
- Program with complex inputs require complex fuzzing

#### Smarter examples
- **Blackbox**
  - Mutation-based
  - Generation-based
- **Greybox**
  - Evolutionary
- **Whitebox**
  - Symbolic execution

##### Mutation-based

Apply random mutations to known valid inputs.

- E.g. observe network traffic, emulate & modify

Pros & cons:

- No assumptions on input required
- May not cover all paths
- May retrace previously tested paths
- Heavily biased on initial input
- Most mutations are not actually interesting, this is only a "smart" bruce force attempt

##### Generation-based

Generate semi-well-formed input based on the input grammar

 - Unused or reserved packet fields
 - Exhaustive search on specific fields
 - Incorrect lengths, zero lengths

Guess grammar:

- from file format
- from network protocol

Pros & cons:

- More accurate than mutation-based
- Only works if some specifications are found
- Needs an ad-hoc generator for each program, each format

##### Evolutionary *(grey box)*

An improved mutation-based approach which aims to learn interesting mutations from how previous input is processed, in order to minimize wasted effort/time.

`afl` *(American Fuzzy Lop)* mutates valid input, then test whether it produced a new execution path. If it does, it's recorded as *useful*, else it's discarded. Goal is to increase code coverage of all executions caused by the inputs.

- if source is available, use a modified compiler
- else run code in an emulator

##### Whitebox approach

Symbolic execution: analyze code and input values that will trigger as many branches as possible.

- SMT solvers can simplify and resolve constraints
- Microsoft's `SAGE` *(Scalable Automated Guided Execution)* can generate such constraints
- Very expensive
- *State explosion* due to loops
