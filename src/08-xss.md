# Cross site scripting 

- XSS *(Cross Site Scripting)* abuses trust that users have in a website
- CSRF *(Cross Site Request Forgery)* abuses trust the website has in the user

## XSS *(HTML injection)*

Maliciously crafting input that is going to be echoed back into a webpage, by injecting/hiding html into it (possibly including javascript or other executable code!).

### Stored XSS attack

An attacker inserts custom html/js into a third-party webpage that is considered safe by users, potentially to then attack the visitors of said webpage (or simply to disrupt the service of the website). 

### Reflected XSS attack

Attacker crafts a malicious URL to a vulnerable website (potentially including handcrafted query strings or JS inserted in URL), then tempts users (e.g. sending emails) to click the link.

## CSRF

A reflected XSS attack can also be used for a form of CSRF called **session riding**, i.e. the attacker then exploits potential implicit authentication (e.g. through cookie, saved login params, etc) on the page he linked the user to, in order to carry out some operation in his stead (e.g. a bank transfer).

This is an authentication problem, as well as a TOCTOU problem.

## Countermeasures

- input validation could be useful, but in this case the problem is actually the **output**
- apply least privilege: turn off scripting, restrict access to cookies & don't use them for sensitive data etc.

## Other webpage problems

- hidden forms
- cookies and other client-side data
  -  should always be re-validated.
  -  may leak confidential information.
  -  since it's stored client-side, may be maliciously manipulated, therefore dangerous to trust

