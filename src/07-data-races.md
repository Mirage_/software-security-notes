# Race conditions *(data races)*

Another common cause of problems, both related and unrelated to security, are data races, i.e. a situation where two or more **threads** independently try to **access the same variable** in a given time interval, and at least one acess is a write.
Race conditions are particularly hard to prevent because they are *non-deterministic* - as such, failed attacks won't be noticed until they actually succeed.

> **Root problem**
> Most instructions are *not* **atomic** operations and may be **interleaved** in unexpected ways with instructions from other threads.

Data races can also happen "implicitly" due to aliasing, e.g. when multiple objects try to free a shared resource.

**Locking** objects can prevent races, but has considerable performance implications, and there is risk of **deadlock**.


## TOCTOU *(Time Of Check, Time Of Use)*

Non-atomic check and use: a precondition is evaluated to be true, but gets invalidated before the completion of the job that required it, potentially invalidating its output as well as its security guarantees.

> **Example**
> `mkdir` is not atomic: creates directory as root, *then* change ownership to the invoking user. An attack could insert a symlink between the two steps to gain access to any file *[how?]*.

## Thread safety

A multi-thread program is thread-safe if its behaviour can be understood as the **interleaving of its threads**.
This is false e.g. for any Java program that has data races, since in that case semantic is undefined.

## File access

Files should be accessed by **file handles** or **descriptors** rather than *file names*, as they may point to different and unintended files at different moments in time - and this is prone to exploits.

It is usually a bad idea to create files in public locations like `/tmp`, especially with predictable filenames: unintended programs could easily read them and possibly figure out information they should have no access to.
