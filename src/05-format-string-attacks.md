# Format string attacks

> 900+ known vulnerabilities involving format strings according to USA.

An attack, invented in the 2000s, which consists in calling a program by passing as parameters one or a series of special symbols. It can result in a **buffer overflow** if the program accesses its parameters in unsafe ways (e.g. without properly typing them as strings).

This attack is made possible by the fact that, in C, it is *not* possible to detect **missing function arguments**. If this happens to a format string, the program may leak or write undesired data from/to memory: the attack thus consists in crafting input parameters for the program in such a way that this condition may happen.

For example, this will read 4 bytes from the stack when trying to interpret `d` due to missing argument (an integer to print):

```c
printf("a string %d");
```

Even assuming no such code is actually ever written by mistake, a program containing an instruction like

```c
printf(argv[i]);
```
could be made to behave like the former, by passing as program argument a string containing `%d` in order to leak stack data. 
Alternatively, `%n` (writes the number of bytes written so far) could be used top write to the location specified from a parameter read from the stack, *including the stack itself* (**stack overflow**!); since this number keeps increasing even if the buffer is full, this can be used to write potentially *anything*, *anywhere*.
Finally, a long format string such as `"%s%s`[...]`"` could be used to crash the program, or potentially the system, by bus error or segmentation fault as the program may try to read from forbidden locations (e.g. system memory).

To solve this problem, program arguments (and indeed all kinds of input) should be interpreted as a **constant** string (which prevents the program from attempting to resolve format string characters), for example:

```c
printf(%s,argv[i])
```

### Noteworthy library functions

- `sprintf(str, format, ...)` is particularly vulnerable, as it can be exploited by format string attack to rewrite the stack with user supplied input by buffer overflow.
- A safer alternative is `snprintf( str, size, format, ...)`, but the output of that may or may *not* be null-terminated depending on the platform.
- `asprintf(ret, format, ...)` can be used to get a pointer to a buffer large enough to hold the formatted string.

### DOs

- Always specify format strings with their parameters
- If possible, make them constant, rather than retrieving them from memory (or worse, user input)
- Use static tools that scan for format string functions called with data strings, e.g. `Pscan`
- If necessary, use `FormatGuard`, a tool for compiling C with additional code inserted to attempt to prevent problems of this kind.

