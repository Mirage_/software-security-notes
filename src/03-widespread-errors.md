# Most widespread errors & vulnerabilities

Often they allow attackers to completely take over the system.

1. Improper neutralization of special characters in a SQL command (**SQL injection**).
1. Improper neutralization of special characters in a OS command (**OS command injection**).
1. Buffer copy without checking the input size (classic **buffer overflow**).
1. Improper neutralization of input during web page generations (**Cross site scripting**), in particular allowing attackers to inject Javascript code into web app-generated pages.
1. **Missing authentication** for critical functions.
1. **Missing authorization** altogether, often because software intended for private use is re-purposed for public use.
1. **Hard coded credentials**, retrievable through reverse-engineering, or obvious.
1. Failing to encrypt **sensitive data**, either completely missing or relying on *in-place* encryption, which exposes the data if the encryption fails.
1. Unrestricted **upload of files** of dangerous type (e.g. the extension is hidden or false, or presumed safe types actually being unsafe, e.g. pdfs).
1. Reliance of **untrusted inputs** in security-related decisions.
1. Executing software with **unnecessary privileges**. 
1. Cros-site request forgery (**CRSF**), i.e. tricking users into activating a request to a site, possibly executing operations that require his privileges.
1. Failing to limit pathnames for a used-created file to allowed directories only (**path traversal**).
1. Downloading and utilizing code without an **integrity check**.
1. **Incorrect authorization**, i.e. present but vulnerable. For example, storing permissions client-side.
1. Inclusion of functionality from **untrusted control sphere**.
1. Incorrect **permission assignment** for critical resources, for example assigning permissions for all possible operations of a program at start, rather than when actually needed.
1. Use of unsafe functions from libraries.
1. Use of vulnerable **cryptographic algorithms**, either custom-made & unproven, or obsolete.
1. Incorrect calculation of **buffer size** (in particul forgetting to account for the string termination characters).
1. Insufficient restriction of **repeated authentication** attempts.
1. **URL redirection** to untrusted sites.
1. Unchecked **format strings**. 
1. **Integer Overflow**, which may also enable buffer overflows.
1. One way **hash without salt** (this means that any hash collision unintentionally allows access to the other accounts with the same hash).

> **Example**: simplest buffer overflow in C
> Writing to out-of-bounds indexes of an array, possibly overriding critical information and causing unexpected behaviour when that data is read, potentially including the execution of malicious code. This could be resolved by checking bounds at runtime, but unlike more modern languages, C does not do it for efficiency reasons.


## Other common problems 

- **dangling pointers** (e.g. aliasing a variable then freeing the location through an alias, then attempting to read the original variable)
- **memory leaks** (e.g. freeing a pointer to the first element in a linked list without freeing the rest of the list)

