# Buffer overflows

Buffer overflows occur whenever a program is allowed to write more data in a buffer (commonly, an array) than it can contain, causing it to overwrite adjacent memory locations.

Depending on data races or other conditions, buffer overflows may happen only *some* of the time, making it harder to locate and thus prevent.

Computers divide memory between stack and other data:
- High memory addresses: stack, which includes the **return address**
- Low memory addresses: heap (dynamic data), static data, then program code.

Depending on where the allocated buffer resides there can be:

- **Stack overflow**: you can fairly easily know the addresses of data stored there, can be used to execute custom code
- **Heap overflow**: hard to know in advance the adresses, therefore hard to exploit in order to execute custom code, but easy to crash the machine or otherwise prevent correct execution

However,if the memory is full, a heap overflow could still manage to write data outside the intended segment and onto the stack section, thus potentially overriding the return address and causing the execution of arbitrary code.


Declaring two arrays in the same line can actually be more vulnerable to buffer overflows than using different lines, as in this case the system will assign adjacent memory locations, thus making it very easy to exploit the code with a buffer overflow if there is a vulnerable point (e.g. function reading input).

A buffer overflow could also be used by an authenticated user to inject commands into system functions in order to have them executed as root, despite the user not having root privileges.

A better language than C, in security terms, could:

- prevent writing out of bounds of arrays (i.e. **dynamic bound-checking**)
- prevent implicit conversion from `signed` to `unsigned int`
- prevent `malloc` calls from failing yet returning null (instead of an error)
- etc

## Examples

Wrong | Correct
-|-
`gets()` will read until EOL/EOF. | `fgets(buf, size, sdtin)` will only read `size` chars
`strcpy(src, dest)` does not check for available space, and assumes src is null-terminated.| Use `strncpy(dest, src, sizeof(dest) - 1)` and then manually null-terminate destination string (`dest[sizeof(dest) - 1] = '\0'`).

Or use OpenBSD's `strlcpy(dst, src, sizeofdst)` and `strlcat(dst, src, sizeofdst)`, which will always null-terminate the output, truncating the source if needed (check return value to detect this).


## Integer overflow $\rightarrow$ buffer overflow

Overflows can also be used to attack programs that concatenate strings from `stdin`. In fact, even if the programmer properly checks that the output (including a final `\0`) fits into the buffer size, an attacker could supply a valid string as `s1` and a string `s2` so long (e.g. 0xFFFFFFFF, ~4 billion) that it overflows the variable containing it, making its len appear  as "0" (or other low value). This way, (`s1`+`s2`) will pass the size check despite being huge, therefore overflowing the buffer when eventually stored.

## Type mismatch $\rightarrow$ Integer underflow $\rightarrow$ buffer overflow

Another danger is *implicit **casts** from signed to unsigned* types.
For example, when computing the length of a string, removing 1 from the total to account for string termination may cause an ***underflow*** if the len was actually 0, since the output is a len and thus cast to `unsigned int`.


## Countermeasures

### OS tools 

- Buffer overflows exploit the structure of memory allocation, therefore they could be prevented by changing it, or making it unpredictable/random (*Address Space Layout Randomization*)
    - However, redesigning OSs is usually not feasible for a common software project
- Disable code execution from the stack (*NOEXEC*) 
- With hardware support it is possible to generate interrupts when data is accessed outside of some wanted boundaries
- **Intrusion Detection** systems could detect anomalies in program behaviour and kill the process
  - False positives/negatives
  - Vulnerable to DoS

### Compiler tools

- Adding bound checking to compilers like `gcc`
    - Does not require modification of source code
    - Can increase compilation time significantly (e.g. 2x)
- Protecting the **return pointer** to specifically prevent return address hijacking (*stack guards*)
    - Still vulnerable to denial of service
    - Might be bypassed by hijacking other function pointers (*trampolining*), so that custom code is executed when program tries to call those.

#### Stack guards

An element (introduced by `StackGuard`) usually inserted into the code by the compiler, that is placed right before the return address. It can be checked before actually interpreting the return address, so that execution can be halted if it is found to have been modified.
However, the guard should be chosen with care: early versions (*canary*) used a hardcoded value, and therefore the attacker could simply overflow a buffer by injecting a string specifically crafted to preserve the guard but change the return address.

Other similar technologies have been made:
  
- `PointGuard` also protects function pointers
- `Stack Smashing Protection` (IBM) also tries to smartly reorder elements on the stack
- `Stackshield` uses a dedicated stack for return addresses and can disallow function pointers to data segment

*None of those are perfect*: it may still be possible to locate and override the "master" canary value to fool the check, or use heap overflows, or corrupt data without executing custom code.

### Analysis tools

- Software that searches code for unsafe library functions
    - Only useful against standard library functions
    - Many false positives

### Programming language tools

- **Higher-level languages** are much less susceptible to buffer overflows, however:
    - Using different languages may increase develpment costs
    - Most require distributable run-time environments
    - No low-level access, which in some cases may be actually needed due to machine constraints or performance requirements (e.g. embedded programming).
- **Safer buffers and library functions** (e.g. some dedicated C++ objects, bsd's or Microsoft's string libraries)
    - Much safer than standard C string/array handling
    - Existing code may need to be modified
    - Behaviour may *not* be the desired one (e.g. truncation, exceptions, buffer is moved and grows)
    - May impose such requirements or performance cost that one might as well use a higher-level language
    - May be less portable

### *Find-and-patch* methods

- **Software patches**
  - Released by vendors when a vulerability is found, therefore vulnerability remains until the patch becomes available.
  - Very effective for the target vulnerability, but only for users that keep system up-to-date. 
  - Being software, may also introduce *new* vulnerabilities.
- **External components** that attempt to block execution of known exploits (anti-virus)
  - Only effective against known attacks
  - Must be updated costantly

### Better software engineering practices

- **Testing**
  - Can find most buffer overflows if done properly
  - It is very expensive, to the point that it's often *economically more convenient* to simply allow the risk of buffer overflows.
  - Might not be possible to properly test vendor code (e.g. source code not available).
  - Good at finding abnormal program behaviour, less good versus **data corruption** 
- **Code inspections**
  - May find overflows that testing didn't find
  - Expensive and time consuming
  - Requires different people to be meaningful
  - Obviously requires source code
- **Documentation** of *vendor code*
  - Often components are reused for contexts the code was not originally meant for, in which they may be unsafe. 
  - Well-documented code is expensive to produce, therefore expensive to acquire. 
  - Proprietary code may intentionally hide some information to prevent **reverse-engineering** (*code obfuscation*).
  