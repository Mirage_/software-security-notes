# Code injection

This attacks consists in tricking a program into executing custom code by passing cleverly constructed input that (like format strings) mixes code and data, this time using special characters that cause a *context evaluation change* by the OS, DBM or other environment.

In the OS, this can be exploited by passing as strings characters that terminate a statement, then a custom statement, e.g. given a file `example.sh`:

```bash
#!/bin/sh
A = $1
eval "ls $A"
```

An attacker could simply run:
```bash
./example.sh ".; [any cmd]"
```

Commands in **backticks** are particularly easy to inject: all the attacker has to do is pass \`[cmd]\` to some program that calls another program (e.g. `grep`) on such input without neutralizing the backticks. If this happens, [cmd] will be executed, before resolving the containing command, with the original program's permissions - potentially even `root`.

## Filename injection

When prompted to name a file, malicious users could attempt to access (read, delete, modify) files outside the **scope** of the current program, if input is not properly validated. 

The most trivial example would be inserting an absolute path to some system resource; even if absolute paths were blocked, an attacker could input a relative path like `../../[...]file.txt` to traverse the filesystem in the opposite direction, and access any file the current program has privilege to access.
Since all resources are files in UNIX, the attacker could also inject the path to a usb-key or other dangerous external device of his choosing, potentially executing whatever it carries.

To limit potential damage from such an attack, it is a good idea to restrict programs to run in the minimum required privileges, but this does not stop the attack completely.
A proper defense would be to use `chroot` **jails** for any program that accept files named by users.

## SQL injection

If input is not validated, it is possible to pass strings capable of executing unintended or even arbitrary SQL code, potentially leaking private data from the DB or even deleting entire tables.
This is usually done by using characters that signal the **end of the current query** at arbitrary points, for example by commenting out security checks in the query (`WHERE` statements), or placing a trivially true statement in `OR` with them, or using `UNION`.

A variation on this theme is a **function call** injection, in which the attacker abuses one of the many built-in SQL functions to have the database execute some code (e.g. connect to a malicious website).

Additionally, Google `Code Search` can easily be used to find websites that are still vulnerable to this.

### Blind SQL injection

Writing queries in attempt to **extract information** on the database itself, potentially to be exploited subsequently.

> **Example**
> Many DBMs have some sort of function unique to them, which the attacker could use (in succession) to figure out which dialect, and possibly version, the system is running on (useful to know which vulnerability to exploit).

## Defending against Code Injection

The attack technique relies on inserting cleverly fabricated strings including special characters that trigger a context change from data to commands.

Therefore, the main defense is to **sanitize** input data by escaping or removing special characters before executing code based on the input.

### Blacklists

Blacklists of symbols are usually **not inclusive enough**, but even if they are, verification should be executed *multiple* times, in fact until the verification finds no result, since removing a character might make previously harmless code into code bearing a code injection attack.

> **Example**
> Some validation functions checked for all printable symbols that terminate a statement, but forgot to include newlines. Therefore an attack could simply input newline and then list any command.

#### Problems in (SQL) blacklists

- Blacklisting potentially dangerous characters, like those employed to terminate statements, is almost never sufficient, since it is hard to include all possible characters, which vary between SQL dialacts.
-  The attacker could fool the detection through dynamic construction of the dangerous characters with functions lile `char(i)`, encoding through URL query strings etc.
- Simply escaping *all* special characters could also expose the system to code intentionally crafted to be activated when escaped (pre-escaped).
- Blocking trivially true statements like same-number equalities (e.g. `WHERE 1=1`) is almost guaranteed to fail as the attacker could use endless variation on the theme (inequalities, equalities on strings, order comparing strings...).


### Whitelists

A better approach is to to **model** precisely what is an **acceptable input** (e.g. as a *regular expression*): this excludes potential vulnerabilities due to unexpected, or even newly introduced symbols (e.g. language update).
The disadvantage in writing a whitelist is that it could end up being *too* restrictive, but temporarely not allowing access to the system to someone who should have it (= *denial of service*) is much better than allowing it to someone who should *not* have it (= system is compromised).

### Taint mode

Some languages use a system to keep track of which input has been validated. Under this system, all **user input** (and other untrusted data) is considered **tainted** and any computation on such data will also result in tainted output. Usually, there are some functions that can **untaint** data by doing proper verification.
Best practice would be to utilize this approach even for trusted sources, as a change of context (or a vulnerability in the trusted component) might make previusly believed *safe* input to become *unsafe* and bypass the protection.

### Type validation

Can also be useful in addition to whitelists. *(More on this later)*

## Defense against SQL Injection

SQL injection, unlike other command injection attacks, can actually be prevented easily and precisely utilizing **prepared statements**: instead of manually creating a string containing the would-be query, and combining it with user input, the programmer should pre-write the query and then binding **string-typed** variables containing user input. In this way it can't be mistaken as commands no matter which character combination the attacker employs.
There are libraries to facilitate this in most programming languages.

### Best practice

1. Resolve character encoding
2. Cleanse input of unwanted characters
    - Multiple passes may still be needed if checking for combinations of characters
3. Validate type
4. Validate semantics

