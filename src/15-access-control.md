# Compartmentalization 

Divide a system/program into chunks and apply principle of least privilege to each part, in order to **minimize TCB** and reduce the inpact of any undetected vulnerability.

> **Example**
> Chrome uses a process for the browser kernel (with user privileges), one for trusted content (e.g. HTTPS certificate warnings), and one for each tab, which has a separate rendering engine. Additionally, plugins have their own process.

One of the standard way to implement this is **runtime access control** *(**sandboxing**)*.

## OS access control

OS can't distinguish components within a process, therefore cannot provide a granular access control for use within applications.
As such, each process has a *fixed* set of permissions, usually those of the user that started it; if the application needs to execute untrusted code (e.g. a browser extension), it should take care to request temporary **reduction** of permissions. If instead it needs **additional** permissions, it should request the user through the OS.
It may be better to simply spawn a new process for any task that requires different permissions, rather than change and then restore the permissions of the original process. 

OS-based access control has a *huge* TCB and thus is very unreliable.

Some implementations also have obvious **complexity** problems, e.g. Windows XP has 30 permissions, 9 categories of users and 15 kinds of objects (in UNIX, it's respectively 3,3,2).

## Language-level access control

Programming languages can offer access control *within* a process, by **sandboxing modules** and monitoring interactions between them. This provides a measure of security even when dealing with *untrusted code*, but it requires memory safety (else untrusted modules could access memory they have no permission to), and is much harder without type safety. Therefore, it is only available in safe programming languages (other languages have to rely on modularity and employ OS access control, e.g. Chrome).

As any access control method, this requires:

1. permissions
2. **protection domains**, i.e. components
3. policies (associations between permissions and components)

### Stackwalking

`DemandPermission(P)`:
- For each caller on the stack (from top to bottom), check if it lacks permission P (or has disabled it). In that case throw an AccessControlException.
- Check resulting access control context
 
## Hardware-provided support for sandboxing

An alternative (or additional) approach to OS or language-based access-control is to employ dedicated support for sandboxing provided by the underlying architecture, such as running untrusted applications inside virtual machines or containers.
*If* the sandbox technology is secure, this means that the VM's OS/ container's microkernel can be excluded from the TCB. However, this approach is less flexible and granular than language-based access-control, since no stack-walking is possible (the stack is inside the sandbox).

## Enclaves

Enclaves isolate part of the code toghther with its necessary data. 

Use cases:

- Running code on not fully trusted cloud providers
- ?

Example: Intel offers hardware support for enclaves through `SGX`.

## Execution-aware memory protection

Access control scheme based on the value of the **program counter**: dictates that certain memory regions may be accessed ony by specific parts of the code.

## Software attacker models

model|description
-|-
I/O attacker|Uses observable output to craft malicious inputs.
Malicious code attacker|Attacks from within the application using malicious components.
Platform level attacker|Attempts to go *under* application protections by exploiting OS interaction with it.
