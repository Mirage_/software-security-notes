# Program verification

Analyzing program surce code to formally proving that the program satisfies certain properties for **all possible executions**.
Often, defining the formal property to be proved is itself not a trivial task.

Ideal examples:

- program always terminates
- program does not leak any information

Realistic examples:

- absence of runtime exceptions
- absence of undefined behaviour
- program is well-typed

## Formal methods

Sub-field of computer science that utilzes the following requirements in order to perform program verification.

- formal semantics of the programming language
- specification language to express properties
- program logic
- verification tool

### Assertions

A common tool for program verification are **assertions**, properties that *must* hold at specific points in the program.
Verfication is performed by following all possible paths of the program, and checking whether the assertion(s) holds for all of them.
In practice, this can be achieved through two methods:

- weakest precondition (or strongest postcondition)
- symbolic execution

When the program contains a loop, however, it is not possible to follow all possible path. The solution is then to utilize **loop invariants**, i.e. assertions that guarantee that a certain property holds both before and after each loop cycle, in addition to the property guaranteed by the loop termination (as long as the loop *does* terminate). This allows us to split the execution graph into a finite number of segments, which can therefore be checked efficiently.

Stronger assertions are often actually *easier* to prove to be invariant than weaker assertions, because they provide a **stronger assumption** that must hold at the beginning of the loop.

Potential difficulties:

- variable shadowing
- data on the heap rather than stack (e.g. static class fields) may be changed by object methods
- data races

### VCGen

Standard approach for program verification:

1. Program is annotated with properties
2. VCG produces a set of logical properties
3. ?

property type | annotation
-|-
pre-conditions |`@ require` 
post-conditions |`@ ensures`

## JML

JML is a formal specification language meant for Java, including preconditions (`requires`), postconditions (`ensures`), and invariants (`invariant`) in a *design-by-contract* style. In JML, an **invariant** is a property that must be satisfied by an object **at rest**, i.e. while it is not being modified by a method (but both *before* and *after*).
JML annotations are special Java comments `/*@ [...] @*/` or `//@ [...]`, which can either be added to java source files or put in separate `.jml` files.
Properties are specified in Java syntax, with the addition of some operators (`\result`, `\forall`, `\exists`, `==>`, `old(`*var*`)` etc).
Some postconditions may use `signals`. Within methods, **assertions** as well as **loop invariants** are also allowed.
Conditions may be combined, but it is preferrable *not to*, in order to get more specific answers from most tools.

Common uses:

- marking references as non-null (`non_null`).
- some tools actually default to `non_null` and instead offer nullable markers (`null`). 
- listing assumptions on input data
- listing exceptions that a method is allowed to throw (`throws`)

### JML tools

- `ESC/Java2`
- `OpenJML`
- `KeY`
- `Krakatoa`
- ...

Most of these tools are not only **non-complete**, but also **not completely sound**. Therefore, some properties may be unverifiable, some may not be efficiently computable, and even in case of a terminating computation, the result may still be wrong in some cases.

### Verifying verifiers

> **Problem**: How to verify the verifier?

This requires **formal semantics** for the programming language in use, which is generally not available except for academic languages.

#### `Coq` theorem prover

Coq is a mathematic proof assistant based on **higher order type theory**, where all functions have corresponding derived types. It includes a functional programming language. It is very expressive, but offers significanly less automation that tools like *SAT verifiers*.

#### Formal description of JVM states

All JVM states can be described by (...?).

#### Verifying type checkers

Operational semantics can be defined in two styles:

-|-
-|----
defensive | VM state includes all type information, and execution performs all type checks at run time.
|
normal | VM trusts code to be well checked and only runs minimum required runtime checks.


## Proof Carrying Code

Original paper by [?] was motivated by the problem of deciding how to trust third-party kernel additions to operating systems.

### Possible solutions to the extension problem

Method|Problems|Requires security policy
--|--|-
Trust the code producer's **digital signature**| Requires trusting that developer, as well as the CA.|no
||
**Runtime monitoring** the application | Logic bombs. Malware that can hide itself when run on emulations.|yes
||
**Formal verification** |Generally complex, except weaker forms (type checking)|yes

#### Runtime Monitoring

- simple
- information flow is not verifiable or enforceable (e.g. can leak info through covert channels)
- expensive (tools, time)

#### Formal verification

- In all cases it prevents some problems *early*, which is desirable
- Type-checking is valuable because of its simplicity, acceptance and great efficacy-to-cost ratio, however it is by no means complete verification.
- Formal program verification  is very expressive and has no runtime overhead, but ..

### Proof Carrying Code *(PCC)*

Introduced by ?, it is based on the fact that for many cases, find proof is hard, but **verifying** it is simple and efficiently computable ^[see P vs NP problems].
In this way, the burden of proof lies primarily on the supplier of the code, who is also the person in the best position to produce it, and maintainers can mostly stick to performing verification.

Pros:

- very expressive
- no runtime overhead
- works at compile or load time
- smaller TCB than traditional program verification, though still relatively large

Cons:

- requires complex formal infrastructure
- proof terms can be huge (e.g. 100 * code size)


## Verificating information flow

### Type-based

Type-based restriction to information flow (similar to *Bell-LaPadula*) have been proposed to implement static verification of information flow. 
They work by assigning security labels (which function as additional types) to variables, e.g. *Low* (= public) and *High* (= confidential); in practice, usually a lactice of more than 2 labels is used.

#### Issues

Guaranteeing soundness versus **information leak** usually requires excessive restrictions that make them impractical: 

- It is very easy to indirectly leak information, through indexes and their acceptable range, when assigning values from confidential variables to unrestriced **arrays** or vice-versa.
- Even with perfectly correct security labeling, it might be possible to extract confidential information by use of **covert channels**:
  - **timing** the execution of particular code sections that depend on hidden values
  - **non-termination** of certain program flows. 
  - **exceptions** raised during execution
  - **non-determinism** due to multi-threading

#### Rules

?

#### Soundness: non-interference

Proving soundness is done by **non-interference** properties which try to capture  the **semantics** of information flow, i.e. the notion of *what can be observed*.

For program states $\mu,v$ we write $\mu ..?$ if $\mu$ and $v$ agree on low variables.

**Non-interference**
[ def?] 
termination-insensitive
termination-sensitive

**Soundness**

\newtheorem{theorem}{Theorem}
\newtheorem{proof}{Proof}

\begin{theorem}[non-interference] : If $y (\vDash) C:t \texttt{ cmd}$, then $C$ does not leak information.
\end{theorem}

\begin{proof}
By induction on the structure of $C$.
\end{proof}

#### Countermeasures to covert channels

- Non-termination: by posing that all guards must be of *low* value, but this is extremely restrictive.
- Non-determinism: **probabilistic information flow** analysis.
- ?

### Declassification

A mechanism implemented by **less strict** forms of information flow control which allows select methods to declassify certain kinds of information that is necessary for real programs, e.g.:

- for confidentiality, output of an encryption operation may be public though it depends on secret data.
- for integrity, validated input may be considered reliable even though the data source is unreliable.

### Implementations

- Static enforcement
  - `JFlow/Jif` for Java (requires serious effort)
  - Several code analysis tools include *some* data flow analysis, usually not completely sound and never complete.
- Dynamic enforcement:
  - ?
  - ?


