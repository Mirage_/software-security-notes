# The problem of secure software

- Security is often a **secondary concern**
- **Tradeoff** between usability & functionalities, and security
- Natively **unsafe languages** (e.g. C)
- **Software complexity** (large APIs, huge infrastructures)

## Evolution in tackling software security

1. **Do nothing**, eventually sell patched versions
2. **Regular patching**
3. Pre-emptive **pen-testing**
4. **Static analysis** on code
5. **Training** programmers to understand common security problems
6. Implement **guards** against typical abuse cases

## Actors involved in security

- Stakeholders
- Their assets
- Threats to the assets
- Potential attackers

> Security is about imposing **countermeasures** to reduce **risk** to assets to *acceptable* levels.

## Objectives of security

- Confidentiality
- **Integrity**

#### Phases of defense

- Prevention
- Detection
- Reaction


## General security problems of software:

- input
- language expressivity (OS, SQL, HTML/JS, C's format strings,PHP, Word/Excel macros...)


#### DoS attacks through input:

- **zip** bomb (40kB $\rightarrow$ 4GB when extracted)
- **XML** bomb (1kB $\rightarrow$ 3GB when XML parser expands recursive definitions) [*billions laugh attack*]

#### Complex file formats

Can often be exploited due to being manipulated through C/C++ code:

- MPEG
- JPEG
  -  in Windows [[CVE-2004-0200](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0200)]
  -  in OpenJDK decoder (Java) [[CVE-2014-2421](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-2421)]
- PDF
- MP3

#### PHP vulnerability examples:

- local file inclusion (**LFI**)
- remote file inclusion (**RFI**) if server is poorly configured

**Countermeasure**: don't use user input to construct a value, rather select from a set of legitimate values based on input. However, this is very inflexible.

#### X509 Certificates

Can contain multiple **Common Names** (i.e. domain names).

- Different APIs handle this by different conventions (return *first*, *last*, *all*).
- Use of null-terminator in CNs may cause problems


## Recurring themes

1. **Dynamic** behaviour, particularly **executable** content
2. Dangerous **characters**/**words** due to language *semantics*
3. **Strings**, particularly **concatenation**


## Input

### Validation techniques

Ranked by *safety*, inversely related to *flexibility*.

1. Indirect selection
2. White-listing
3. Black-listing

#### Actions for illegal input

1. **Reject** input
2. Try to **sanitize** it *(need to **repeat** until none found)*
   1. **Remove** dangerous character combinations
   2. **Escape** them

#### Processing input

1. Parsing/lexing
2. Interpreting

Problems:

1. Imprecise notion of input validity
2. Parser differentials
3. *Shotgun parsers* that incrementally parse&interprete [*Blaster* worm]
4. Unchecked development of input languages (new features, new standards...)

The correct method would be to use a language with opposing features to those above:

1. Accespt precisely defined input languages (**grammars**)
2. Use a parser *generated* from said specification
3. Complete parsing *before* processing the input (e.g. prepared statements instead of dynamically constructed SQL)
4. Keep input language simple
    - ideally, parsers equivalence should be **decidable** (*context-free language* vs *Turing complete*)
    - if T. complete, attacker may be able to do anything through malicious input, if undetected.


## Client vs Server-side validation

- **Security-critical** checks should be done *server-side*
- Some **input validation** can/should be done *client side*
  - JS insertion in URL
  - ?
- Ideally **both**
