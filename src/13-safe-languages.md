# Language safety

Many attacks are made possible by abusing the expressivity of several programming languages, usually when dealing with input.

## Types

Types assert **invariant properties** of elements in a program.
They are necessary to compile source code, as they indicate which binary representation to use for each variable.
They are also useful in verifying that components are being used in the way they're supposed to, as specified by the type annotations: this is called **type checking** and can be done both at compile and at run time.

- Languages without types are usually interpreted instead.
- Weakly-typed languages use types only for compilation
- Strongly typed languages also use them to guarantee type-safety

### Sound type-systems

A type system is **sound** if **representation independence** can be proven for all data types.
This means that given both a *typed* and *untyped* formal definition of the language, they should be proven to be equivalent for any program accepted by the type-checker.

Type-systems can additionally be enriched by features such as:

- annotations for **nullable**/non-nullable types
- alias control
- information flow control (e.g. taint mode)

### Type-safety

A program is *safe* if it hasn't crashed or hanged. It is **type-safe** if it will *always* be safe when well-typed, which implies that type assertions will always hold at runtime.
This also means that data will only be manipulated in ways that are compatible with the data type (e.g. booleans cannot be multiplied, integers cannot be dereferenced).

> **Example**
> C is not type-safe because it performs unsafe type casting implicitly, e.g. `char[8] -> char[]` (array is now unbounded).

Type safety *requires* memory safety.

#### Mini-C

A strongly typed, Turing-complete language with guaranteed type safety.

*[Theorem for Mini-C computations and formalization of the language]*


## Memory safety

1. A program can never access **unallocated** or deallocated memory
   - no segmentatiion faults at runtime
   - in theory, no need for OS memory access control (but only if execution engine is perfect...)
3. Ideally, also cannot access **uninitialized** memory
   - no need to zero-out memory before deallocation to prevent potential inormation leaks

Counterexamples:

- unchecked array bounds
- pointer arithmetic
- null pointers with undefined behaviour in some cases (e.g. dereferencing null pointers in C)
- manual memory management (e.g. dangling pointers, double free, use after free etc.)

## Safe programming language

Programming languages can provide:

- varying levels of **type saftey** depending on expressivity
- memory safety
- **thread safety** (e.g. Rust's compiler)
- exceptions & exception handling
- some form of **access control**: visibility & access restrictions, mechanisms for sandboxing
- some form of **information flow control** (e.g. JFlow)
- **immutability** of primitive values or some objects, e.g. strings
- **safer arithmetic** than C (specify overflow/underflow behaviour, throw an exception, experimental infinite precision numbers)
- good APIs and libraries that promote security (e.g. SQL prepared statements support, safer string libraries for C)

Often, more advanced features depend on previous features: type safety needs mem safety, sandboxing needs mem and type safety.

> At their core, safe programming languages impose **restrictions** in exchange for **abstractions** and **guarantees**.

A p.l. is considered **safe** if:

1. the abstractions it provides can be trusted (**enforced**, cannot be broken)
2. programs have precise **semantics** (meaning), i.e. **no undefined beheaviour** for edge cases
3. program behaviour can be understood in a modular way

A possible ordering of programming language by safety:

1. **memory safe**, typed, **type-safe**: Java, C#, Rust, Go, many functional languages
2. **memory-safe**, **untyped**: LISP, Python and many other intepreted languages
3. **memory-unsafe**, typed, **type-unsafe**: C, C++

The safest languages are usually functional languages because they have **no side-effects**.

### Abstractions

All programming languages provide some abstractions:

- variables and types are abstractions of memory
- statements are abstractions of CPU instructions
- function calls abstract the call stack mechanism

Other abstractions are provided by the OS:

- virtual memory abstracts raw memory
- processes abstract CPU and memory utilization

Abstractions are crucial to **reduce complexity** (which also means reduce potential for bugs), and can also provide security features, but often rely on assumptions which may be broken in edge cases. And when the assumptions are broken, security is often violated. 

### Mechanisms for safety

- compile time checks
- run time checks
- garbage collector that automates memory management
- execution engine with safety features (e.g. JVM)

Without an execution engine, all safety features have to compiled directly into the code, while the engine can provide some checks at runtime without the need to add them to each program.
However, the execution engine has to be considered part of the **trusted code base** (TCB), as any bugs or vulnerabilities therein can potentially invalidate all safety guarantees.

> **Example**
> Type-safety can be broken if the JVM can be confused to think a certain memory location holds data of a different type than what it really has [Java in Netscape 3.0 attack]