# Aliasing
Aliasing means assigning multiple references to the same object, by different names.
It is not necessarily bad, in fact it is often used for:

- efficiency reasons (e.g. alias to middle of a list)
- consistent views between objects that share the object, rather than copies
- useful programming patterns (callbacks, observer...)

Additionally, aliasing creates no problem if the referenced object is not **mutable**: problems arise when the object is mutable through multiple references, since this violates the expectancy that the object will only be modiefied through its class methods.

There are two ways to create potentially unwanted aliases:

- **exporting** a reference: this can easily happen if the class exposes `getX()` methods that return mutable objects.
- **importing** a reference, typically through a class constructor

## Alias control through annotations

In Java (and others), `@rep` annotations can be added to class properties to mark them non-editable through aliases, meaning that access through aliases will be read-only. This is achieved by making type $T$ and `@rep` $T$ **assignment incompatible**.
This technique is important both as documentation of intent and as type-checking aid.

## Immutable objects

Another approach to prevent aliasing problems (as well as others) is the use of immutable objects. In fact, immutable objects are functionally identical to **values**, which are obviously immune to race conditions.
This can have mixed effects on program :

- **efficiency** because immutable objects can be freely shared
- **inefficiency** because any need for changing the object requires creating and assigning a new object

An example of this is using Integer and other wrapper classes, which can be modified only through the class methods.

For additional security, particularly vs race conditions, the keyword **final** can be used: this will make it impossible to assign a new object to the property, but *not* make the object immutable by itself (`@immutable` annotation can be added to classes for that).
