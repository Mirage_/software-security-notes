# Cryptography

[missing 9/12 lesson]

Considered as a weapon by USA, in the past export was forbidden. Rules were relaxed after 2000, but it still requires authorization from NSA (*Terrorist-7* countries are also excluded from export).

Both *designing* and *implementing* secure cryptographic algorithms is very complex and very expensive: publicly trusted libraries should always be preferred.

## Key Exchange

- Key agreement: two paties compute a secret key together (e.g. Diffie-Hellman k.e.).
- Key distribution: one party generates a akey and distributes it through a trusted channel (e.g. a Key Distribution Center).

### Certificates

Keys also need to be authenticated, to prevent **man-in-the-middle** attacks. This is done by a hierarchy of **Certification Authorities** (CA).

## Java cryptographic architecture

It's divided in **JCA** (main architecture) and **JCE** (extensions). [^ .NET has a similar structure]

feature | provided by.
-|-
Algorythm independence | **Engine** classes
Implementation independence | **Provider**-based architecture
Implementation interoperability | **Transparent** and **opaque** data types

### *Engine* classes

- They are abstractions for cryptographic services, providing alrgorythm-independent APIs. 
- Istances are created through factory methods.
- Specific implementations use inheritance from base classes.

Opaque vs transparent:

- **Opaque**: the representation of a component is completely delegated to the implementation object, including key material.
- **Transparent**: the API has visibility over cryptographic parameters, including key material.

### *Provider* classes

They hold details about which class implements which algorithm.

### Additional support

- Secure streams for bulk enc/dec
- **Signed objects** for integrity-checking serialized objects
- **Sealed objects** for creating encrypted serialized objects
- Certificate helpers, but *no* certificate-creation facility (possibly to prevent easy set-up of "rougue" CAs)
- Keystores (e.g. keychains).

### Example

[Too long, see slides]